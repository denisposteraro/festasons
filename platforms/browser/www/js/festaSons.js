var urlBaseSomSimulator = "file:///data/data/com.adobe.phonegap.app/files/phonegapdevapp/www/sound/";
var urlBaseSomAndroid = "file:///android_asset/www/sound/";
var base = document.getElementById('base');
var btnAtual = 1;
var novobtn;
var btn;
var btnN;
var somInicial;
var click_time;
var last_click_time;

document.addEventListener("deviceready", onDeviceReady, false);
function onDeviceReady() {
	baseCredito = document.getElementById('baseCredito');
	baseJogo = document.getElementById('baseJogo');
	var oculto=false;
	var tipo = false;

	if(document.getElementById("btnDir") != null){
		document.getElementById("btnDir").onclick = function() {
			novobtn =  btnAtual + 1;
			btnN = document.getElementById('btn'+novobtn);
			if(btnN == null){
				erro();
			}else{
				btn = document.getElementById('btn'+btnAtual);
				btn.classList.add("hidden");
				btnAtual = novobtn;
				btnN.classList.remove("hidden");
				playSound(btnN.getAttribute("red")+".mp3");
			}	
			
		};	
	}
	

	if(document.getElementById("btnEsq")!=null){
		document.getElementById("btnEsq").onclick = function() {
			novobtn =  btnAtual - 1;
			btnN = document.getElementById('btn'+novobtn);
			if(btnN == null){
				erro();
			}else{
				btn = document.getElementById('btn'+btnAtual);
				btn.classList.add("hidden");
				btnAtual = novobtn;
				btnN.classList.remove("hidden");
				playSound(btnN.getAttribute("red")+".mp3");
			}

		};
	}

	if(document.getElementById("modoVisual")!=null){
		document.getElementById("modoVisual").onclick = function() {
			
			click_time = new Date().getTime();
			if (click_time != null && (click_time - last_click_time) < 500) {
				if(oculto){
					oculto=false;
					if(base!=null){
						base.className = "base-botoes";
					}else if(baseCredito!=null){
						baseCredito.className = "base-creditos";
					}else if(baseJogo!=null){
						baseJogo.className="base-jogo";
					}

				}else{
					oculto=true;
					if(base!=null){
						base.className = "base-botoes ocultar";
					}else if(baseCredito!=null){
						baseCredito.className = "base-creditos ocultar";
					}else if(baseJogo!=null){
						baseJogo.className="base-jogo ocultar";
					}
				}
			}else{
				playSound("ocultar.mp3");
			}
			last_click_time = click_time;   
		};
	}
	

	if(somInicial != null){
		playSound(somInicial);	
	}


 }

function btnMenu(redirecionar,audio){
	click_time = new Date().getTime();
	if (click_time && (click_time - last_click_time) < 500) {
		window.location.assign(redirecionar);
	}else{
		playSound(audio+".mp3");
	}
	last_click_time = click_time;   
}

function btnMenu(redirecionar,audio){
	click_time = new Date().getTime();
	if (click_time && (click_time - last_click_time) < 500) {
		window.location.assign(redirecionar);
	}else{
		playSound(audio+".mp3");
	}
	last_click_time = click_time;   
}

function fechar(){
	click_time = new Date().getTime();
	if (click_time && (click_time - last_click_time) < 500) {
		navigator.app.exitApp();
	}else{
		playSound("sair.mp3");
	}
	last_click_time = click_time;   
}


var media_audio = null;
var media_audio_sim = null;

function playSound(som){
	setTimeout(playAudio(som), 2000);
}

function playAudio(som){
	if(media_audio){
		media_audio.stop();
		media_audio.release();
	}
	media_audio = new Media(urlBaseSomAndroid+som,null,playSoundSim(som));
	
	if(som == "found.mp3" || som == "voce_errou.mp3"){
		media_audio.play();
		media_audio.setVolume(0.2);
	}else{
		media_audio.play();
		media_audio.setVolume(1.0);
	}
	
}

function playSoundSim(som){
	if(media_audio_sim){
		media_audio_sim.stop();
		media_audio_sim.release();
	}
	media_audio_sim = new Media(urlBaseSomSimulator+som);
	if(som == "found.mp3" || som == "voce_errou.mp3"){
		media_audio_sim.play();
		media_audio_sim.setVolume(0.2);
	}else{
		media_audio_sim.play();
		media_audio_sim.setVolume(1.0);
	}
}

var media_erro = null;
var media_erro_sim = null;


function erro(){
	if(media_erro){
		media_erro.stop();
		media_erro.release();
	}
	media_erro = new Media(urlBaseSomAndroid+"erro.mp3",null,erroS());
	media_erro.play();
	media_erro.setVolume(0.1);
}

function erroS(){
	if(media_erro_sim){
		media_erro_sim.stop();
		media_erro_sim.release();
	}
	media_erro_sim = new Media(urlBaseSomSimulator+"erro.mp3");
	media_erro_sim.play();
	media_erro_sim.setVolume(0.1);
}





